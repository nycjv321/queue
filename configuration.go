package queue

import "fmt"

type QueueConnectionSettings struct {
	Username, Password, Hostname string
	Port                         int
}

type QueueConfiguration struct {
	Name string
}


func (connectionSettings QueueConnectionSettings) String() string {
	return fmt.Sprintf("amqp://%v:%v@%v:%v",
		connectionSettings.Username,
		connectionSettings.Password,
		connectionSettings.Hostname,
		connectionSettings.Port)
}
