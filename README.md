# Hare
A RabbitMQ publishing/consumer client. 

## About
This project is merely a wrapper around the official RabbitMQ Golang Client. 
For the really cool stuff check out the official client. 

This project provides a simple abstraction around Queue & Connection configuration. 
Additionally, it provides simplified queue channel management through Consumer/Publisher "workers" 

Connection configuration is provided via `QueueConnectionSettings`. This struct allows you to 
define the internal connection configuration. Currently, the connection is managed as a singleton 
between a Publisher/Consumer and workers. If you want to increase the number of connections either 1\) 
create multiple publishers/consumers or 2\) (if you think it would be a valuable feature) submit a pull request.

## Importing this project

    import (
       	. "gitlab.com/nycjv321/queue"
    )


## Publishing Example 

    var publisher PublisherWorker
    
    func initializePublisher() {
    	hare := CreatePublisher(&QueueConfiguration{Name: "test"},
    	&QueueConnectionSettings{
    		Username: "guest",
    		Password: "guest",
    		Hostname: "localhost", Port: 5672})
    	if worker, err := hare.CreateWorker(); err == nil {
    		publisher = worker
    	} else {
    		panic(err)
    	}
    }
    
    // Accept a message as input for publishing
    // Package the message for publishing using your prefered
    // serialization format (e.g. bytes/string/json/protobuf/MessagePack)
    func publish(message *Message) (error) {
    	if body, err := serialize(message); err == nil {
    		if err := publisher.Publish(body, "text/plain"); err == nil {
    			fmt.Printf("Published \"%v\"", string(body))
    			return nil
    		} else {
    			panic(err)
    		}
    	} else {
    		return err
    	}
    }
    
## Consuming Example

    var consumer ConsumerWorker
    
    func initializeConsumer() {
        hare := CreateConsumer(&QueueConfiguration{Name: "test"},
            &QueueConnectionSettings{
                Username: "guest",
                Password: "guest",
                Hostname: "localhost", Port: 5672})
        if worker, err := hare.CreateWorker(); err == nil {
            consumer = worker
        } else {
            panic(err)
        }
    }
    
    // Accept a message as input for publishing
    // Package the message for publishing using your prefered
    // serialization format (e.g. bytes/string/json/protobuf/MessagePack)
    func consume() {
        go consumer.Consume(func(messages <-chan amqp.Delivery) {
            for message := range messages {
                if episode, err := deserialize(message.Body); err == nil {
                    log.Printf("Received a message: %s", episode)
                } else {
                    log.Print(err)
                }
            }
        })
    }