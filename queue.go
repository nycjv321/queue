package queue

import "github.com/streadway/amqp"

type Queue struct {
	Connection    *amqp.Connection
	QueueSettings *QueueConfiguration
}