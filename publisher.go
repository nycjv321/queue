package queue

import (
	"github.com/streadway/amqp"
)

type Publisher struct {
	Queue *Queue
}


func (publisher Publisher) Close() error {
	return publisher.Queue.Connection.Close()
}


func (publisher Publisher) CreateWorker() (PublisherWorker, error) {
	queue := publisher.Queue
	channel, err := queue.Connection.Channel()

	q, err := channel.QueueDeclare(
		queue.QueueSettings.Name, // name
		false,                    // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)

	return PublisherWorker{Channel: channel, Queue: &q}, err
}

func CreatePublisher(queueConfiguration *QueueConfiguration, connectionSettings *QueueConnectionSettings) *Publisher {
	if conn, err := amqp.Dial(connectionSettings.String()); err == nil {
		return &Publisher{Queue: &Queue{Connection: conn, QueueSettings: queueConfiguration}}
	} else {
		panic(err)
	}
}


