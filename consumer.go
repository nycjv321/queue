package queue

import (
	"github.com/streadway/amqp"
)

type Consumer struct {
	Queue *Queue
}

func (consumer Consumer) Close() error {
	return consumer.Queue.Connection.Close()
}

func CreateConsumer(queueConfiguration *QueueConfiguration, connectionSettings *QueueConnectionSettings) *Consumer {
	if conn, err := amqp.Dial(connectionSettings.String()); err == nil {
		return &Consumer{Queue: &Queue{Connection: conn, QueueSettings: queueConfiguration}}
	} else {
		panic(err)
	}
}

func (consumer Consumer) CreateWorker() (ConsumerWorker, error) {
	queue := consumer.Queue
	channel, err := queue.Connection.Channel()

	q, err := channel.QueueDeclare(
		queue.QueueSettings.Name, // name
		false,                    // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)

	return ConsumerWorker{Channel: channel, Queue: &q}, err
}
