package queue

import (
	"github.com/streadway/amqp"
)

type Worker struct {
	Channel *amqp.Channel
	Queue   *amqp.Queue
}

type PublisherWorker = Worker
type ConsumerWorker = Worker

func (worker PublisherWorker) Publish(body []byte, contentType string) error {
	channel := worker.Channel
	return channel.Publish(
		"",                // exchange
		worker.Queue.Name, // routing key
		false,             // mandatory
		false,             // immediate
		amqp.Publishing{
			ContentType: contentType,
			Body:        body,
		})
}


func (worker ConsumerWorker) Consume(consumer func (<-chan amqp.Delivery)) error {
	if msgs, err := worker.Channel.Consume(
		worker.Queue.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	); err == nil {
		consumer(msgs)
	} else {
		return err
	}
	return nil
}


